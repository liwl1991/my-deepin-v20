#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# ***************************************************************************
# 本脚本发送markdown文件至www.cnblogs.com
# --file 指定要上传的markdown博客文件
# --upload 指定要上传的(多个)图片
# 其他说明:
#   {'python3':"xmlrpc.client",'python2':"xmlrpclib"}
#   user:博客园账号
#   password:博客园密码
#   appkey:MetaWeblog的密钥(2022/06/21,博客园MetaWeblog接口变化,需要用密钥访问,查看地址:https://i.cnblogs.com/settings#enableServiceAccess)
#   url:MetaWeblog的访问地址。[博客园-账号设置-博客设置-其他设置-允许MetaWeblog]
# ***************************************************************************

import sys
import xmlrpc.client 
import mimetypes
import os
import json
import subprocess
from optparse import OptionParser

#这里配置user,password,appkey,url
CNBLOGS_USER_INFO = {
    'user':"博客园帐号",
    'password':"博客园密码",
    'appkey':"MetaWeblog的密钥"
    'url':"MetaWeblog的访问地址",
}

class CnblogsAPI():
    """
    读取用户的:url,帐号,密码
    """
    def __init__(self):
        self.__user = CNBLOGS_USER_INFO.get('user')
        #self.__password = CNBLOGS_USER_INFO.get('password')
        self.__password = CNBLOGS_USER_INFO.get('appkey') #改为获取appkey，代替password
        self.__url = CNBLOGS_USER_INFO.get('url')
        self.__cnblogs_api = xmlrpc.client.ServerProxy(self.__url)
        self.__user_info = self.__cnblogs_api.blogger.getUsersBlogs('',self.__user,self.__password)
        self.__user_blogid = self.__user_info[0].get('blogid')

    def get_all_blogs(self):
        """
        获取全部博客:{title:postid}
        """
        blogs_title_postid = {}
        #all_blogs = self.__cnblogs_api.metaWeblog.getRecentPosts(self.__user_blogid,self.__user,self.__password,0)
        all_blogs = self.__cnblogs_api.metaWeblog.getRecentPosts(self.__user_blogid,self.__user,self.__password,365) #查询最近一年的,查询比较慢,适当调增
        for blog in all_blogs:
            blogs_title_postid.update({blog.get('title'):blog.get('postid')})
        return blogs_title_postid

    def __get_post_id(self,post_name):
        """
        通过博客标题获取博客的post_id
        """
        return self.get_all_blogs().get(post_name)

    def __get_mt_keywords(self,post_name):
        """
        获取tag
        """
        return self.__cnblogs_api.metaWeblog.getPost(self.__get_post_id(post_name),self.__user,self.__password).get('mt_keywords')

    def upload_image_get_url(self,image):
        """
        指定图片上传图片,返回图片的url
        """
        with open(image,'rb') as f:
            image_bits = xmlrpc.client.Binary(f.read())
        image_type = mimetypes.guess_type(image)[0]
        image_name = os.path.basename(image)
        url = self.__cnblogs_api.metaWeblog.newMediaObject(self.__user_blogid,self.__user,self.__password,dict(bits=image_bits,name=image_name,type=image_type))
        return url.get('url')

    def edit_post(self,blog_file):
        """
        指定博客文件上传,发送通知到操作系统
        """
        with open(blog_file,'r') as blog:
            blog_content = blog.read()

        post_name = os.path.basename(blog_file).split('.') #修复博客名称有.md后缀问题
        if len(post_name) >= 2: #2022.09.15,修复博客文档名称带有.的文件，比如java.util.ArrayList.md
            post_name = ".".join(post_name[0:-1])
        post_id = self.__get_post_id(post_name)
        mt_keywords = self.__get_mt_keywords(post_name) if post_id else "" #判断tag是否存在,存在添加tag
        if blog_content == "":
            blog_content = "</br>" #修复无法发送空文件的问题
        post_info = {
            'title':post_name,
            'description':blog_content,
            'categories':['','[Markdown]'],#默认不发送到博客园首页候选区,默认markdown格式,2022/06/29:随笔分类不再使用[],而是字符串''
            'mt_keywords':mt_keywords,#发送时添加tag
        }

        if post_id:
            return self.__cnblogs_api.metaWeblog.editPost(post_id,self.__user,self.__password,post_info,True)
        else:
            return self.__cnblogs_api.metaWeblog.newPost(self.__user_blogid,self.__user,self.__password,post_info,True)


if __name__ == "__main__":
    #脚本参数
    parser = OptionParser()
    parser.add_option("-f","--file",dest="filename")
    parser.add_option("-u","--upload",dest="image",action="append")
    (options,args) = parser.parse_args()
    my_cnblogs = CnblogsAPI()
    #获取markdown博客
    markdown_blog = options.filename
    if options.filename:
        if my_cnblogs.edit_post(markdown_blog):
            subprocess.run('notify-send -i text-x-markdown "创建/更新博客,成功"',shell=True)
        else:
            subprocess.run('notify-send -i text-x-markdown "创建/更新博客,失败"',shell=True)
    #获取多个图片
    if options.image:
        for img in options.image + args:
            print(my_cnblogs.upload_image_get_url(img))
