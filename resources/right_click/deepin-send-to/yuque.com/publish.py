#!/usr/bin/env python3
#*****************************************************************************************
# 通过API来处理语雀内容
# 存在的问题:
# 1. 无法上传图片。markdown的图片，如果是正确的url能够解析,不正确的url也不能解析
# 2. 上传的内容，如果在线编辑以后，无法再次通过API推送更新。如果要更新，只能通过API更新
# 3. 知识库如果创建后没有文档,通过api上传会放到目录下，如果已经创建过目录，通过API推送会被放到暂存
#****************************************************************************************

import sys
import subprocess
import requests
import json
import time,calendar

#配置修改
YUQUE_ACCOUNT = "xxx" # 账户管理-个人路径:https://www.yunwei.com/xxx
DEFAULT_KB = "xxx" #默认上传的知识库
APP_NAME = "xxx" #应用名称
ACCOUNT_TOKER = "xxx" #修改为Token

HEADERS = {
    'Content-type':"application/json",
    'User-Agent':APP_NAME,
    'X-Auth-Token':ACCOUNT_TOKER,
}

USER_URL = "https://www.yuque.com/api/v2/users/%s" % YUQUE_ACCOUNT
GROUP_URL = "https://www.yuque.com/api/v2/users/%s/groups" % YUQUE_ACCOUNT
KB_URL = "https://www.yuque.com/api/v2/users/%s/repos" % YUQUE_ACCOUNT
DOC_URL = "https://www.yuque.com/api/v2/repos/%s/%s/docs" % (YUQUE_ACCOUNT,DEFAULT_KB)


def read_doc(doc):
    """
    获取markdown内容.
    注:不同于为知笔记,语雀私有协议不需要对markdown文档进行处理,直接读取内容即可。
    """
    content = ""
    with open(doc,'r') as note:
        for line in note.readlines():
            content = content + line
    return content

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("未选择文档")
        sys.exit(0)
    if sys.argv[1] != "--file":
        sys.exit(0)
    FILE = sys.argv[2]
    TITLE = FILE.split('/')[-1]
    CONTENT = read_doc(FILE)
    BODY = {
        'title': TITLE.split('.')[0],
        'slug':calendar.timegm(time.gmtime()),
        'public' :1,
        'format':"markdown",
        'body':CONTENT,
    }

    request_rtn = requests.post(url=DOC_URL,headers=HEADERS,data=json.dumps(BODY))
    if request_rtn.status_code == 200:
        subprocess.run('notify-send -i text-x-markdown "创建语雀笔记成功"',shell=True)
    else:
        subprocess.run('notify-send -i text-x-markdown "创建语雀笔记失败"',shell=True)
