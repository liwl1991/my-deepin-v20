#!/bin/bash
# 本脚本用于配置:自编译新增功能的deepin-terminal

if [ -f /usr/bin/deepin-terminal.${USER} ]
then
    sudo unlink /usr/bin/deepin-terminal.${USER}
fi
sudo cp deepin-terminal   /usr/bin/deepin-terminal.${USER}
if [ $? -eq 0 ]
then
    echo "拷贝deepin-terminal成功"
fi
sudo cp deepin-terminal_zh_CN.qm /usr/share/deepin-terminal/translations/deepin-terminal_zh_CN.qm
if [ $? -eq 0 ]
then
    echo "拷贝翻译成功"
fi
sudo ln -fs /usr/bin/deepin-terminal.${USER} /usr/bin/deepin-terminal
if [ $? -eq 0 ]
then
    echo "创建软链接成功"
fi
#增加可执行权限
sudo chmod +x /usr/bin/deepin-terminal.${USER}
if [ $? -eq 0 ]
then
    echo "修改权限成功"
fi

##拷贝终端配置文件
cp config.conf /home/${USER}/.config/deepin/deepin-terminal/
if [ $? -eq 0 ]
then
    echo "拷贝配置文件成功"
fi
#
##自定义命令
##cp ${RESOURCE_DIR}/command-config.conf /home/${USER}/.config/deepin/deepin-terminal/
##远程管理
##cp ${RESOURCE_DIR}/server-config.conf /home/${USER}/.config/deepin/deepin-terminal/
#
##主题配色。自编译终端修改了Dark和Light的配色参数
sudo cp Dark.colorscheme /usr/share/terminalwidget5/color-schemes/
if [ $? -eq 0 ]
then
    echo "修改深色主题成功"
fi
sudo cp Light.colorscheme /usr/share/terminalwidget5/color-schemes/
if [ $? -eq 0 ]
then
    echo "修改浅色主题成功"
fi
#
##拷贝系统库
sudo cp libterminalwidget5.so.0.14.1 /lib/x86_64-linux-gnu/libterminalwidget5.so.0.14.1.${USER} #这一步有问题，拷贝会失败,找不出问题
if [ $? -eq 0 ]
then
    echo "拷贝库文件成功"
fi
echo "查看库大小"
rtn=$(du -sh /lib/x86_64-linux-gnu/libterminalwidget5.so.0.14.1.${USER}|awk '{print $1}')
if [ "${rtn}" != "804K" ]
then
    sudo cp libterminalwidget5.so.0.14.1 /lib/x86_64-linux-gnu/libterminalwidget5.so.0.14.1.${USER} #上面拷贝失败，再次拷贝正常
fi
sudo ln -fs /lib/x86_64-linux-gnu/libterminalwidget5.so.0.14.1.${USER} /lib/x86_64-linux-gnu/libterminalwidget5.so.0.14.1
if [ $? -eq 0 ]
then
    echo "创建库软链接成功"
fi
