#!/bin/bash
# bug:偶尔发现键盘失灵，系存在ctrl未释放的情况。再按一下ctrl即可。
AW=$(xwininfo -int -id $(xdotool getactivewindow) | grep "Window id"|awk '{print $NF}'|sed 's/\"//'|sed 's/\"//g')
if [[ $AW == "桌面" ]] || [[ $AW == "文件管理器" ]]
then 
    bash -c 'xdotool keyup d key --delay 0 --clearmodifiers Delete'
else
    bash -c 'xdotool keyup d key --delay 0 --clearmodifiers ctrl+D;'
fi
