#!/usr/bin/env bash
HEIGHT=$1
function set_height(){
MODE=$1
if [ -f ~/.local/share/deepin/themes/deepin/"$MODE"/titlebar.ini ]
then
    sed -i "s/height=[0-9]\?\+/height=$HEIGHT/" ~/.local/share/deepin/themes/deepin/"$MODE"/titlebar.ini
else
    mkdir -p ~/.local/share/deepin/themes/deepin/"$MODE"/
touch ~/.local/share/deepin/themes/deepin/"$MODE"/titlebar.ini
cat >>~/.local/share/deepin/themes/deepin/"$MODE"/titlebar.ini <<EOF
[Active]
height=$((HEIGHT))

[Inactive]
height=$((HEIGHT))
EOF
fi
}

set_height "light"
set_height "dark"
