# 1.概述
这是使用Python重新编写的《我的deepin一键变形》脚本。

> 适用于DeepinV20系列(20.5-20.9)

所谓一键变形，也就是一个脚本一站式处理**重新安装deepin操作系统后**的个性化配置操作。

与《我的deepin一键变形》相关视频:

2.[我的deepin一键变形记](https://www.bilibili.com/video/BV1SU4y127ih/)


当前的操作包括：

+ 系统瘦身
 1. 自带应用卸载
 2. 壁纸屏保删除
 3. 多余主题删除
 4. 多余图片删除

+ 个性化设置
 1. 安装字体
 2. 设置桌面壁纸
 3. 背景图片
 4. 用户图像
 5. 启动器样式
 6. 锁屏界面设置

+ 第三方应用定制安装
 1. fcitx5
 2. wechat(wine)
 3. edge(stable)
 4. wps
 5. wiznote(linux)
 6. typora
 7. nutstore
 8. baidunetdisk
 9. vscode

+ 控制中心
 1. 电源选项设置
 2. 圆角大小设置
 3. 增加和修改全局快捷键

+ 任务栏
 1. 设置高效模式 
 2. 托盘显隐设置
 3. 驻留应用设置
 4. 日期格式设置
 5. 插件显隐设置

+ 其他
 1. 替换deepin-terminal为自己编译的版本
 2. 右键添加Markdown模板(显示为:技术文档)
 3. 引导启动界面无主题无延迟(双系统请不要配置)
 4. vimrc,bashrc初始化配置
 5. 右键发送markdown文档到博客园,为知笔记,语雀(需要自己填写相关帐号密码)
 6. 替换dde-control-center为自编译版本(修复部分bug,引导配置界面有问题)
 7. 替换dde-file-manager为自编译版本(支持ctrl+d删除文件)

# 2. 使用方式
+ 下载源码包.
+ 解压后,拷贝目录下的run.py,resources目录到目标机器
+ 先vim run.py查看脚本内容,不想执行的操作，把True改为False
+ 终端执行: python3 run.py -a -y | tee -a init.py.log 
+ 交互执行不要加-y

脚本提供的参数如下:
```
python3 run.py:[-s|-c|-p|-a] [-y]
-a:全部执行;
-s:系统瘦身;
-c:常用软件安装;
-p:个性化配置;
-y:无交互执行
```

# 3. 存在的问题
1. 目前发现输入sudo密码后,可能会在后续的执行过程中继续出现密码认证。这会导致进度提示符号#持续出现
2. 如果默认安装坚果云之后,安装过程可能会有问题。并且成功安装之后,不会对坚果云进行配置。
3. 如果删除了deepin-app-stroe以后，启动器卸载软件时，顶栏提示无icon.需要把对应的deepin-appstore.svg拷贝到保留的主题下面，文件名不要搞错.
4. 自编译程序：
    1. 文件管理器必须在更新系统以后才可以正常使用，否则无法正常使用。或者部署完成以后，再更新系统。
    2. 启动器无法右键卸载应用
    3. 控制中心无法配置启动菜单

# 4. 其他说明
# 授权修改文件
/usr/share/polkit-1/actions/org.libvirt.unix.policy #虚拟化
/usr/share/polkit-1/actions/org.kubuntu.qaptworker3.policy #软件管理器
